#EAN API

This project contains one component:

- ean-api: An java library to handle making calls to [Expedia Affiliate Network API] [EAN].


##Downloading
The output jars are hosted in Maven Central:

    group: com.geeoz.ean
    artifact: ean-api

##Building the Library
This will build the library, publish it to a local artifactory repository, then resolve the dependencies of the project. 

###Requirements

- [Java JDK (1.7+)] [java]
- [Apache Maven (3.0.4+)] [apache-maven]

###Steps

1. Download the source code from git: `git clone git@bitbucket.org:geeoz/ean-api.git`
            
2. Open a command line in the cloned directory: `cd ./ean-api`

3. Now run the maven to build the ean-api: `mvn clean install`
    - The library has now been built, published to a local artifactory repository, the app has had its dependencies resolved.
    

## More Help

More assistance can be found on our [wiki for this project] [git-project-wiki] and our [developer hub] [dev-hub].

[EAN]: http://www.expediaaffiliate.com/ "Expedia Affiliate Network"
[dev-hub]: http://developer.ean.com "EAN Developer Hub"
[git-project]: https://bitbucket.org/geeoz/ean-api.git "ean-api project"
[git-project-wiki]: https://bitbucket.org/geeoz/ean-api/wiki "ean-api project wiki"
[java]: http://www.oracle.com/technetwork/java/javase/downloads/index.html "Java"
[apache-maven]: http://maven.apache.org/download.cgi "Apache Maven"
