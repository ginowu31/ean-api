/*
 * Copyright 2013 Geeoz Software
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.geeoz.ean.ws.rs;

import com.ean.wsapi.hotel.v3.HotelListRequest;
import com.ean.wsapi.hotel.v3.LocaleType;
import com.ean.wsapi.hotel.v3.Room;
import com.ean.wsapi.hotel.v3.RoomGroup;
import org.junit.Test;

import java.io.UnsupportedEncodingException;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;

/**
 * Provider for marshalling of {@code application/x-www-form-urlencoded}
 * entity type from {@link com.ean.wsapi.hotel.v3.BaseRequest} child instance.
 *
 * @author Alex Voloshyn
 * @version 1.0 7/13/2013
 */
public class JsonFormBodyWriterTest {
    /**
     * Verify that api key was converted.
     */
    @Test
    public void shouldBuildStringWithApiKey()
            throws UnsupportedEncodingException {
        final HotelListRequest request = new HotelListRequest();
        request.setApiKey("apiKey");
        final StringBuilder builder = new StringBuilder();
        JsonFormBodyWriter.writeTo(request.getClass(), request, builder);
        assertThat("Result strings should be the same.",
                builder.toString(), is(equalTo("&apiKey=apiKey&cid=0")));
    }

    /**
     * Verify that locale was converted correctly.
     */
    @Test
    public void shouldBuildStringWithLocale()
            throws UnsupportedEncodingException {
        final HotelListRequest request = new HotelListRequest();
        request.setLocale(LocaleType.EN_US);
        final StringBuilder builder = new StringBuilder();
        JsonFormBodyWriter.writeTo(request.getClass(), request, builder);
        assertThat("Result strings should be the same.",
                builder.toString(), is(equalTo("&cid=0&locale=en_US")));
    }

    /**
     * Verify room format for REST.
     * <p/>
     * The REST format compacts the values from the previous elements into
     * a comma-delimited list. To declare a room and its occupants, use the
     * following format:
     * <p/>
     * &room[room number, starting with 1]=
     * [number of adults],
     * [comma-delimited list of children's ages]
     * <p/>
     * For example, to declare that a room has one adult and two children ages
     * 5 and 12, you would send &room1=1,5,12. There is no separate declaration
     * for the number of children - each age value is assumed to belong to
     * a child.
     */
    @Test
    public void shouldBuildStringWithRoomFormatForREST() {
        final HotelListRequest request = new HotelListRequest();
        final RoomGroup group = new RoomGroup();
        request.setRoomGroup(group);

        final Room room1 = new Room();
        room1.setNumberOfAdults(1);
        group.getRoom().add(room1);

        final StringBuilder builder1 = new StringBuilder();
        JsonFormBodyWriter.writeTo(request.getClass(), request, builder1);
        assertThat("Result strings should be the same.",
                builder1.toString(), is(equalTo("&cid=0&room1=1")));

        final Room room2 = new Room();
        room2.setNumberOfAdults(2);
        room2.setNumberOfChildren(2);
        room2.getChildAges().add(5);
        room2.getChildAges().add(12);
        group.getRoom().add(room2);

        final StringBuilder builder2 = new StringBuilder();
        JsonFormBodyWriter.writeTo(request.getClass(), request, builder2);
        assertThat("Result strings should be the same.",
                builder2.toString(),
                is(equalTo("&cid=0&room1=1&room2=2,5,12")));

    }

    /**
     * Verify that array was converted.
     */
    @Test
    public void shouldBuildStringWithArray()
            throws UnsupportedEncodingException {
        final HotelListRequest request = new HotelListRequest();
        request.getHotelIdList().add((long) 1);
        request.getHotelIdList().add((long) 2);
        final StringBuilder builder = new StringBuilder();
        JsonFormBodyWriter.writeTo(request.getClass(), request, builder);
        assertThat("Result strings should be the same.",
                builder.toString(), is(equalTo("&cid=0&hotelIdList=1,2,")));
    }
}
