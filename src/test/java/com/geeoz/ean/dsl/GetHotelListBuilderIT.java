/*
 * Copyright 2013 Geeoz Software
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.geeoz.ean.dsl;

import com.geeoz.ean.Expedia;
import com.geeoz.ean.ws.EanWsException;
import com.ean.wsapi.hotel.v3.EanWsError;
import com.ean.wsapi.hotel.v3.ErrorCategory;
import com.ean.wsapi.hotel.v3.ErrorHandling;
import com.ean.wsapi.hotel.v3.HotelListResponse;
import com.ean.wsapi.hotel.v3.LocaleType;
import com.ean.wsapi.hotel.v3.SearchRadiusUnitType;
import com.ean.wsapi.hotel.v3.ServerInfo;
import com.ean.wsapi.hotel.v3.SortType;
import org.junit.Test;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.UUID;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;

/**
 * Retrieve a list of hotels by location or a list of specific hotelIds.
 * <p/>
 * This method can be used to return hotels with available rooms for a provided
 * date range, or to return a list of all active properties within the specified
 * location without any availability information.
 * <p/>
 * This method supports multiple filters and methods of specifying the desired
 * location to allow a variety of front-end search options, such as searching by
 * airport code or a visualization on a map.
 *
 * @author Alex Voloshyn
 * @version 1.0 7/13/2013
 */
public class GetHotelListBuilderIT {
    /**
     * Verify that response contains detailed error message.
     */
    @Test
    public void shouldRetrieveDetailedErrorMessage() {
        final String uuid = UUID.randomUUID().toString();
        try {
            Expedia.getHotelList()
                    .locale(LocaleType.EN_US)
                    .customer(uuid, uuid, uuid)
                    .call();
        } catch (EanWsException fault) {
            // CHECK ERROR MESSAGE PROPERTIES
            final EanWsError error = fault.getError();
            assertThat("Itinerary IDs should be the same.",
                    error.getItineraryId(), is(equalTo((long) -1)));
            assertThat("Error handling behaviours should be the same.",
                    error.getHandling(), is(equalTo(ErrorHandling.RECOVERABLE)));
            assertThat("Error categories should be the same.",
                    error.getCategory(),
                    is(equalTo(ErrorCategory.DATA_VALIDATION)));
            assertThat("Exception condition IDs should be the same.",
                    error.getExceptionConditionId(), is(equalTo(-1)));
            final String message = "Data in this request could not be validated: "
                    + "Specified city could not be resolved as valid location.";
            assertThat("Presentation messages should be the same.",
                    error.getPresentationMessage(), is(equalTo(message)));
            assertThat("Verbose messages should be the same.",
                    error.getVerboseMessage(), is(equalTo(message)));

            // SERVER INFO ATTRIBUTES
            final ServerInfo info = error.getServerInfo();
            assertThat("Server time should not be empty.",
                    info.getServerTime(), is(notNullValue()));
            assertThat("Timestamp should not be empty.",
                    info.getTimestamp(), is(notNullValue()));
            assertThat("Server instance should not be empty.",
                    info.getInstance(), is(notNullValue()));
        }
    }

    /**
     * Verify city/state/country search method results.
     */
    @Test
    public void shouldRetrieveHotelsForCSCSearch() throws EanWsException {
        final String uuid = UUID.randomUUID().toString();
        final Calendar calendar = new GregorianCalendar();
        calendar.add(Calendar.DATE, 1);
        final Date tomorrow = calendar.getTime();
        calendar.add(Calendar.DATE, 1);
        final Date afterTomorrow = calendar.getTime();

        final DateFormat format = new SimpleDateFormat("MM/dd/YYYY");

        final HotelListResponse response =
                Expedia.getHotelList()
                        .locale(LocaleType.EN_US)
                        .currency("USD")
                        .customer(uuid, uuid, uuid)
                        .dates(
                                format.format(tomorrow),
                                format.format(afterTomorrow))
                        .room(1)
                        .search("Kiev", null, null)
                        .numberOfResults(20)
                        .include(false, false)
                        .call();

        assertThat("Customer session IDs should be the same.",
                response.getCustomerSessionId(), is(equalTo(uuid)));
        assertThat("Hotel list should exists.",
                response.getHotelList(), is(notNullValue()));
        assertThat("Hotel summary should exists.",
                response.getHotelList().getHotelSummary().size(),
                is(greaterThan(5)));
    }

    /**
     * Verify a free text destination string search method results.
     */
    @Test
    public void shouldRetrieveHotelsByDestinationStringSearch()
            throws EanWsException {
        final String uuid = UUID.randomUUID().toString();
        final Calendar calendar = new GregorianCalendar();
        calendar.add(Calendar.DATE, 1);
        final Date tomorrow = calendar.getTime();
        calendar.add(Calendar.DATE, 1);
        final Date afterTomorrow = calendar.getTime();

        final DateFormat format = new SimpleDateFormat("MM/dd/YYYY");

        final HotelListResponse response =
                Expedia.getHotelList()
                        .locale(LocaleType.EN_US)
                        .currency("USD")
                        .customer(uuid, uuid, uuid)
                        .dates(
                                format.format(tomorrow),
                                format.format(afterTomorrow))
                        .room(1)
                        .searchByDestination("Kiev,UA")
                        .numberOfResults(20)
                        .include(false, false)
                        .call();

        assertThat("Customer session IDs should be the same.",
                response.getCustomerSessionId(), is(equalTo(uuid)));
        assertThat("Hotel list should exists.",
                response.getHotelList(), is(notNullValue()));
        assertThat("Hotel summary should exists.",
                response.getHotelList().getHotelSummary().size(),
                is(greaterThan(5)));
    }

    /**
     * Verify a destinationId search method results.
     */
    @Test
    public void shouldRetrieveHotelsByDestinationIdSearch()
            throws EanWsException {
        final String uuid = UUID.randomUUID().toString();
        final Calendar calendar = new GregorianCalendar();
        calendar.add(Calendar.DATE, 1);
        final Date tomorrow = calendar.getTime();
        calendar.add(Calendar.DATE, 1);
        final Date afterTomorrow = calendar.getTime();

        final DateFormat format = new SimpleDateFormat("MM/dd/YYYY");

        final HotelListResponse response =
                Expedia.getHotelList()
                        .locale(LocaleType.EN_US)
                        .currency("USD")
                        .customer(uuid, uuid, uuid)
                        .dates(
                                format.format(tomorrow),
                                format.format(afterTomorrow))
                        .room(1)
                        .searchById("47CC53D2-98A6-486B-8106-8783749CA665")
                        .numberOfResults(20)
                        .include(false, false)
                        .call();

        assertThat("Customer session IDs should be the same.",
                response.getCustomerSessionId(), is(equalTo(uuid)));
        assertThat("Hotel list should exists.",
                response.getHotelList(), is(notNullValue()));
        assertThat("Hotel summary should exists.",
                response.getHotelList().getHotelSummary().size(),
                is(greaterThan(5)));
    }

    /**
     * Verify a list of hotelIds search method results.
     */
    @Test
    public void shouldRetrieveHotelsByHotelIdListSearch()
            throws EanWsException {
        final String uuid = UUID.randomUUID().toString();
        final Calendar calendar = new GregorianCalendar();
        calendar.add(Calendar.DATE, 1);
        final Date tomorrow = calendar.getTime();
        calendar.add(Calendar.DATE, 1);
        final Date afterTomorrow = calendar.getTime();

        final DateFormat format = new SimpleDateFormat("MM/dd/YYYY");

        final HotelListResponse response =
                Expedia.getHotelList()
                        .locale(LocaleType.EN_US)
                        .currency("USD")
                        .customer(uuid, uuid, uuid)
                        .dates(
                                format.format(tomorrow),
                                format.format(afterTomorrow))
                        .room(1)
                        .searchByHotelIdList(152560, 127092)
                        .numberOfResults(20)
                        .include(false, false)
                        .call();

        assertThat("Customer session IDs should be the same.",
                response.getCustomerSessionId(), is(equalTo(uuid)));
        assertThat("Hotel list should exists.",
                response.getHotelList(), is(notNullValue()));
        assertThat("Hotel summary should exists.",
                response.getHotelList().getHotelSummary().size(),
                is(equalTo(2)));
    }

    /**
     * Verify a geographical area search method results.
     */
    @Test
    public void shouldRetrieveHotelsByGeographicalIdSearch()
            throws EanWsException {
        final String uuid = UUID.randomUUID().toString();
        final Calendar calendar = new GregorianCalendar();
        calendar.add(Calendar.DATE, 1);
        final Date tomorrow = calendar.getTime();
        calendar.add(Calendar.DATE, 1);
        final Date afterTomorrow = calendar.getTime();

        final DateFormat format = new SimpleDateFormat("MM/dd/YYYY");

        final HotelListResponse response =
                Expedia.getHotelList()
                        .locale(LocaleType.EN_US)
                        .currency("USD")
                        .customer(uuid, uuid, uuid)
                        .dates(
                                format.format(tomorrow),
                                format.format(afterTomorrow))
                        .room(1)
                        .searchByArea(
                                47.613247f,
                                -122.19644f,
                                5, SearchRadiusUnitType.KM,
                                SortType.NO_SORT
                        )
                        .numberOfResults(20)
                        .include(false, false)
                        .call();

        assertThat("Customer session IDs should be the same.",
                response.getCustomerSessionId(), is(equalTo(uuid)));
        assertThat("Hotel list should exists.",
                response.getHotelList(), is(notNullValue()));
        assertThat("Hotel summary should exists.",
                response.getHotelList().getHotelSummary().size(),
                is(greaterThan(5)));
    }
}
