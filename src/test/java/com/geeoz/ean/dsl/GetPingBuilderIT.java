/*
 * Copyright 2013 Geeoz Software
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.geeoz.ean.dsl;

import com.geeoz.ean.Expedia;
import com.geeoz.ean.ws.EanWsException;
import com.ean.wsapi.hotel.v3.PingResponse;
import com.ean.wsapi.hotel.v3.ServerInfo;
import org.junit.Test;

import java.util.UUID;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;

/**
 * Send a ping request to our API servers to determine if service is available
 * in the event of a suspected outage or ISP issue, or to obtain EAN's Unix
 * server time when troubleshooting issues with signature authentication.
 *
 * @author Alex Voloshyn
 * @version 1.0 7/13/2013
 */
public class GetPingBuilderIT {
    /**
     * Verify that service call returns an echo that was send in request.
     * <p/>
     * SOAP service should be used.
     */
    @Test
    public void shouldRetrieveTheSameEcho() throws EanWsException {
        final String echo = UUID.randomUUID().toString();
        final PingResponse response = Expedia.getPing().echo(echo).call();
        assertThat("Echo from response should be equal to input.",
                response.getEcho(), is(equalTo(echo)));

        // SERVER INFO ATTRIBUTES
        final ServerInfo info = response.getServerInfo();
        assertThat("Server time should not be empty.",
                info.getServerTime(), is(notNullValue()));
        assertThat("Timestamp should not be empty.",
                info.getTimestamp(), is(notNullValue()));
        assertThat("Server instance should not be empty.",
                info.getInstance(), is(notNullValue()));
    }
}
