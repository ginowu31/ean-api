/*
 * Copyright 2013 Geeoz Software
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.geeoz.ean.dsl;

import com.ean.wsapi.hotel.v3.BaseResponse;
import com.ean.wsapi.hotel.v3.HotelBaseRequest;

/**
 * Base hotel request builder for EAN API requests.
 *
 * @param <T> type of the builder
 * @param <R> type of the request
 * @param <S> type of the response
 * @author Alex Voloshyn
 * @version 1.0 7/13/2013
 */
public abstract class AbstractHotelBuilder<
        T extends AbstractHotelBuilder<T, R, S>,
        R extends HotelBaseRequest,
        S extends BaseResponse>
        extends AbstractBuilder<T, R, S> {
    /**
     * Sets the minor revision used for processing requests and returning
     * responses.
     */
    private static final Integer MINOR_REV =
            Integer.valueOf(API.getProperty("minorRev"));

    /**
     * Hotel base request initialisation with digital sign of the request.
     * <p/>
     * Note:
     * This method call AbstractBuilder#baseInit(BaseRequest) method first of
     * all.
     *
     * @param request hotel base request that should be initialize
     */
    protected static void baseHotelInit(final HotelBaseRequest request) {
        baseInit(request);
        request.setMinorRev(MINOR_REV);
    }

    /**
     * Add minor revision parameter to EAN API request.
     *
     * @param value minor revision value
     * @return builder instance
     */
    public final T minorRev(final Integer value) {
        request().setMinorRev(value);
        return self();
    }
}
