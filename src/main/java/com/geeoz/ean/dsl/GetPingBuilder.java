/*
 * Copyright 2013 Geeoz Software
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.geeoz.ean.dsl;

import com.ean.wsapi.hotel.v3.PingRequest;
import com.ean.wsapi.hotel.v3.PingResponse;

/**
 * Send a ping request to our API servers to determine if service is available
 * in the event of a suspected outage or ISP issue, or to obtain EAN's Unix
 * server time when troubleshooting issues with signature authentication.
 *
 * @author Alex Voloshyn
 * @version 1.0 7/13/2013
 */
public final class GetPingBuilder extends
        AbstractHotelBuilder<GetPingBuilder, PingRequest, PingResponse> {
    /**
     * The ping request does not have any required elements or parameters. You
     * may optionally send the echo parameter as described below, otherwise the
     * request may be sent empty.
     */
    private final PingRequest request;

    /**
     * An empty constructor with request initialization.
     */
    public GetPingBuilder() {
        request = new PingRequest();
        baseHotelInit(request);
    }

    /**
     * Sets the value of the echo property.
     *
     * @param value echo for server
     * @return builder instance
     */
    public GetPingBuilder echo(final String value) {
        request().setEcho(value);
        return self();
    }

    @Override
    protected PingResponse response() {
        return getHotelServices().getPing(request());
    }

    @Override
    protected PingRequest request() {
        return request;
    }

    @Override
    protected GetPingBuilder self() {
        return this;
    }
}
