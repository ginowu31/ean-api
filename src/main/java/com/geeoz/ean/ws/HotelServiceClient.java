/*
 * Copyright 2013 Geeoz Software
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.geeoz.ean.ws;

import com.ean.wsapi.hotel.v3.AlternatePropertyListRequest;
import com.ean.wsapi.hotel.v3.AlternatePropertyListResponse;
import com.ean.wsapi.hotel.v3.HotelInformationRequest;
import com.ean.wsapi.hotel.v3.HotelInformationResponse;
import com.ean.wsapi.hotel.v3.HotelItineraryRequest;
import com.ean.wsapi.hotel.v3.HotelItineraryResponse;
import com.ean.wsapi.hotel.v3.HotelListRequest;
import com.ean.wsapi.hotel.v3.HotelListResponse;
import com.ean.wsapi.hotel.v3.HotelPaymentResponse;
import com.ean.wsapi.hotel.v3.HotelRateRulesRequest;
import com.ean.wsapi.hotel.v3.HotelRateRulesResponse;
import com.ean.wsapi.hotel.v3.HotelRoomAvailabilityRequest;
import com.ean.wsapi.hotel.v3.HotelRoomAvailabilityResponse;
import com.ean.wsapi.hotel.v3.HotelRoomCancellationRequest;
import com.ean.wsapi.hotel.v3.HotelRoomCancellationResponse;
import com.ean.wsapi.hotel.v3.HotelRoomImageRequest;
import com.ean.wsapi.hotel.v3.HotelRoomImageResponse;
import com.ean.wsapi.hotel.v3.HotelRoomReservationRequest;
import com.ean.wsapi.hotel.v3.HotelRoomReservationResponse;
import com.ean.wsapi.hotel.v3.HotelServices;
import com.ean.wsapi.hotel.v3.HotelServicesImplService;
import com.ean.wsapi.hotel.v3.LocationInfoRequest;
import com.ean.wsapi.hotel.v3.LocationInfoResponse;
import com.ean.wsapi.hotel.v3.PingRequest;
import com.ean.wsapi.hotel.v3.PingResponse;
import com.ean.wsapi.hotel.v3.V3HotelPaymentRequest;

import javax.jws.WebParam;

/**
 * Hotel services SOAP client.
 *
 * @author Alex Voloshyn
 * @version 1.0 7/13/2013
 */
public final class HotelServiceClient implements HotelServices {

    @Override
    public HotelRoomImageResponse getRoomImage(
            @WebParam(name = "HotelRoomImageRequest", targetNamespace = "")
            final HotelRoomImageRequest request) {
        return getHotelServices().getRoomImage(request);
    }

    @Override
    public HotelListResponse getList(
            @WebParam(name = "HotelListRequest", targetNamespace = "")
            final HotelListRequest request) {
        return getHotelServices().getList(request);
    }

    @Override
    public HotelRoomAvailabilityResponse getAvailability(
            @WebParam(
                    name = "HotelRoomAvailabilityRequest",
                    targetNamespace = "")
            final HotelRoomAvailabilityRequest request) {
        return getHotelServices().getAvailability(request);
    }

    @Override
    public HotelRoomCancellationResponse getCancellation(
            @WebParam(
                    name = "HotelRoomCancellationRequest",
                    targetNamespace = "")
            final HotelRoomCancellationRequest request) {
        return getHotelServices().getCancellation(request);
    }

    @Override
    public LocationInfoResponse getGeoLocation(
            @WebParam(name = "LocationInfoRequest", targetNamespace = "")
            final LocationInfoRequest request) {
        return getHotelServices().getGeoLocation(request);
    }

    @Override
    public AlternatePropertyListResponse getAlternateProperties(
            @WebParam(
                    name = "AlternatePropertyListRequest",
                    targetNamespace = "")
            final AlternatePropertyListRequest request) {
        return getHotelServices().getAlternateProperties(request);
    }

    @Override
    public HotelRoomReservationResponse getReservation(
            @WebParam(
                    name = "HotelRoomReservationRequest",
                    targetNamespace = "")
            final HotelRoomReservationRequest request) {
        return getHotelServices().getReservation(request);
    }

    @Override
    public HotelItineraryResponse getItinerary(
            @WebParam(name = "HotelItineraryRequest", targetNamespace = "")
            final HotelItineraryRequest request) {
        return getHotelServices().getItinerary(request);
    }

    @Override
    public HotelRateRulesResponse getRateRules(
            @WebParam(name = "HotelRateRulesRequest", targetNamespace = "")
            final HotelRateRulesRequest request) {
        return getHotelServices().getRateRules(request);
    }

    @Override
    public HotelPaymentResponse getPaymentInfo(
            @WebParam(name = "HotelPaymentRequest", targetNamespace = "")
            final V3HotelPaymentRequest request) {
        return getHotelServices().getPaymentInfo(request);
    }

    @Override
    public PingResponse getPing(
            @WebParam(name = "PingRequest", targetNamespace = "")
            final PingRequest request) {
        return getHotelServices().getPing(request);
    }

    @Override
    public HotelInformationResponse getInformation(
            @WebParam(name = "HotelInformationRequest", targetNamespace = "")
            final HotelInformationRequest request) {
        return getHotelServices().getInformation(request);
    }

    /**
     * Retrieve an instance of the hotel services.
     *
     * @return SOAP implementation of the hotel services
     */
    private HotelServices getHotelServices() {
        return new HotelServicesImplService().getHotelServicesImplPort();
    }
}
