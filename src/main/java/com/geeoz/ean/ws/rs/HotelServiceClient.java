/*
 * Copyright 2013 Geeoz Software
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.geeoz.ean.ws.rs;

import com.ean.wsapi.hotel.v3.AlternatePropertyListRequest;
import com.ean.wsapi.hotel.v3.AlternatePropertyListResponse;
import com.ean.wsapi.hotel.v3.BaseRequest;
import com.ean.wsapi.hotel.v3.GetAlternatePropertiesResponse;
import com.ean.wsapi.hotel.v3.GetAvailabilityResponse;
import com.ean.wsapi.hotel.v3.GetCancellationResponse;
import com.ean.wsapi.hotel.v3.GetGeoLocationResponse;
import com.ean.wsapi.hotel.v3.GetInformationResponse;
import com.ean.wsapi.hotel.v3.GetItineraryResponse;
import com.ean.wsapi.hotel.v3.GetListResponse;
import com.ean.wsapi.hotel.v3.GetPaymentInfoResponse;
import com.ean.wsapi.hotel.v3.GetPingResponse;
import com.ean.wsapi.hotel.v3.GetRateRulesResponse;
import com.ean.wsapi.hotel.v3.GetReservationResponse;
import com.ean.wsapi.hotel.v3.GetRoomImageResponse;
import com.ean.wsapi.hotel.v3.HotelInformationRequest;
import com.ean.wsapi.hotel.v3.HotelInformationResponse;
import com.ean.wsapi.hotel.v3.HotelItineraryRequest;
import com.ean.wsapi.hotel.v3.HotelItineraryResponse;
import com.ean.wsapi.hotel.v3.HotelListRequest;
import com.ean.wsapi.hotel.v3.HotelListResponse;
import com.ean.wsapi.hotel.v3.HotelPaymentResponse;
import com.ean.wsapi.hotel.v3.HotelRateRulesRequest;
import com.ean.wsapi.hotel.v3.HotelRateRulesResponse;
import com.ean.wsapi.hotel.v3.HotelRoomAvailabilityRequest;
import com.ean.wsapi.hotel.v3.HotelRoomAvailabilityResponse;
import com.ean.wsapi.hotel.v3.HotelRoomCancellationRequest;
import com.ean.wsapi.hotel.v3.HotelRoomCancellationResponse;
import com.ean.wsapi.hotel.v3.HotelRoomImageRequest;
import com.ean.wsapi.hotel.v3.HotelRoomImageResponse;
import com.ean.wsapi.hotel.v3.HotelRoomReservationRequest;
import com.ean.wsapi.hotel.v3.HotelRoomReservationResponse;
import com.ean.wsapi.hotel.v3.HotelServices;
import com.ean.wsapi.hotel.v3.LocationInfoRequest;
import com.ean.wsapi.hotel.v3.LocationInfoResponse;
import com.ean.wsapi.hotel.v3.PingRequest;
import com.ean.wsapi.hotel.v3.PingResponse;
import com.ean.wsapi.hotel.v3.V3HotelPaymentRequest;

import javax.jws.WebParam;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;

import static java.lang.String.format;

/**
 * Hotel services REST client.
 *
 * @author Alex Voloshyn
 * @version 1.0 7/13/2013
 */
public final class HotelServiceClient implements HotelServices {
    /**
     * Base hotel service URL.
     */
    private static final String URL =
            "http://api.ean.com/ean-services/rs/hotel/v3/%s?";
    /**
     * Base secure hotel service URL.
     */
    private static final String SECURE_URL =
            "https://book.api.ean.com/ean-services/rs/hotel/v3/%s?";
    /**
     * Hotel room image service URL.
     */
    private static final String ROOM_IMAGES_URL = format(URL, "roomImages");
    /**
     * Hotel list service URL.
     */
    private static final String LIST_URL = format(URL, "list");
    /**
     * Room availability service URL.
     */
    private static final String AVAILABILITY_URL = format(URL, "avail");
    /**
     * Hotel room cancellation service URL.
     */
    private static final String CANCELLATION_URL = format(URL, "cancel");
    /**
     * Geo location service URL.
     */
    private static final String GEO_LOCATION_URL = format(URL, "geoSearch");
    /**
     * Alternate properties service URL.
     */
    private static final String ALTERNATE_PROPERTIES_URL =
            format(URL, "altProps");
    /**
     * Hotel room reservation service URL.
     */
    private static final String RESERVATION_URL = format(SECURE_URL, "res");
    /**
     * Hotel itinerary service URL.
     */
    private static final String ITINERARY_URL = format(URL, "itin");
    /**
     * Hotel rate rules service URL.
     */
    private static final String RULES_URL = format(URL, "rules");
    /**
     * Hotel payment info service URL.
     */
    private static final String PAYMENT_INFO_URL = format(URL, "paymentInfo");
    /**
     * Ping service URL.
     */
    private static final String PING_URL = format(URL, "ping");
    /**
     * Hotel information service URL.
     */
    private static final String INFORMATION_URL = format(URL, "info");

    @Override
    public HotelRoomImageResponse getRoomImage(
            @WebParam(name = "HotelRoomImageRequest", targetNamespace = "")
            final HotelRoomImageRequest request) {
        return get(ROOM_IMAGES_URL, request, GetRoomImageResponse.class)
                .getHotelRoomImageResponse();
    }

    @Override
    public HotelListResponse getList(
            @WebParam(name = "HotelListRequest", targetNamespace = "")
            final HotelListRequest request) {
        return get(LIST_URL, request, GetListResponse.class)
                .getHotelListResponse();
    }

    @Override
    public HotelRoomAvailabilityResponse getAvailability(
            @WebParam(
                    name = "HotelRoomAvailabilityRequest",
                    targetNamespace = "")
            final HotelRoomAvailabilityRequest request) {
        return get(AVAILABILITY_URL, request, GetAvailabilityResponse.class)
                .getHotelRoomAvailabilityResponse();
    }

    @Override
    public HotelRoomCancellationResponse getCancellation(
            @WebParam(
                    name = "HotelRoomCancellationRequest",
                    targetNamespace = "")
            final HotelRoomCancellationRequest request) {
        return get(CANCELLATION_URL, request, GetCancellationResponse.class)
                .getHotelRoomCancellationResponse();
    }

    @Override
    public LocationInfoResponse getGeoLocation(
            @WebParam(name = "LocationInfoRequest", targetNamespace = "")
            final LocationInfoRequest request) {
        return get(GEO_LOCATION_URL, request, GetGeoLocationResponse.class)
                .getLocationInfoResponse();
    }

    @Override
    public AlternatePropertyListResponse getAlternateProperties(
            @WebParam(
                    name = "AlternatePropertyListRequest",
                    targetNamespace = "")
            final AlternatePropertyListRequest request) {
        return get(ALTERNATE_PROPERTIES_URL,
                request, GetAlternatePropertiesResponse.class)
                .getAlternatePropertyListResponse();
    }

    @Override
    public HotelRoomReservationResponse getReservation(
            @WebParam(
                    name = "HotelRoomReservationRequest",
                    targetNamespace = "")
            final HotelRoomReservationRequest request) {
        return post(RESERVATION_URL, request, GetReservationResponse.class)
                .getHotelRoomReservationResponse();
    }

    @Override
    public HotelItineraryResponse getItinerary(
            @WebParam(name = "HotelItineraryRequest", targetNamespace = "")
            final HotelItineraryRequest request) {
        return get(ITINERARY_URL, request, GetItineraryResponse.class)
                .getHotelItineraryResponse();
    }

    @Override
    public HotelRateRulesResponse getRateRules(
            @WebParam(name = "HotelRateRulesRequest", targetNamespace = "")
            final HotelRateRulesRequest request) {
        return get(RULES_URL, request, GetRateRulesResponse.class)
                .getHotelRateRulesResponse();
    }

    @Override
    public HotelPaymentResponse getPaymentInfo(
            @WebParam(name = "HotelPaymentRequest", targetNamespace = "")
            final V3HotelPaymentRequest request) {
        return get(PAYMENT_INFO_URL, request, GetPaymentInfoResponse.class)
                .getHotelPaymentResponse();
    }

    @Override
    public PingResponse getPing(
            @WebParam(name = "PingRequest", targetNamespace = "")
            final PingRequest request) {
        return get(PING_URL, request, GetPingResponse.class).getPingResponse();
    }

    @Override
    public HotelInformationResponse getInformation(
            @WebParam(name = "HotelInformationRequest", targetNamespace = "")
            final HotelInformationRequest request) {
        return get(INFORMATION_URL, request, GetInformationResponse.class)
                .getHotelInformationResponse();
    }

    /**
     * Send a GET request to server.
     *
     * @param url      RESTful service URL
     * @param request  an EAP API request
     * @param response an EAP API response
     * @param <T>      type of the EAP API response
     * @return an EAP API response instance
     */
    private <T> T get(final String url,
                      final BaseRequest request,
                      final Class<T> response) {
        final Client client = ClientBuilder.newClient();
        final StringBuilder builder = new StringBuilder(url);
        JsonFormBodyWriter.writeTo(request.getClass(), request, builder);
        return client.target(builder.toString())
                .request(MediaType.APPLICATION_JSON_TYPE).get(response);
    }

    /**
     * Send a POST request to server.
     *
     * @param url      RESTful service URL
     * @param request  an EAP API request
     * @param response an EAP API response
     * @param <T>      type of the EAP API response
     * @return an EAP API response instance
     */
    private <T> T post(final String url,
                       final BaseRequest request,
                       final Class<T> response) {
        final Client client = ClientBuilder.newClient()
                .register(JsonFormBodyWriter.class);
        return client.target(url)
                .request(MediaType.APPLICATION_JSON_TYPE)
                .post(Entity.entity(
                        request, MediaType.APPLICATION_FORM_URLENCODED_TYPE),
                        response);
    }
}
